package org.openpnp.machine.reference.driver;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeoutException;

import javax.swing.Action;

import org.openpnp.gui.support.PropertySheetWizardAdapter;
import org.openpnp.gui.support.Wizard;
import org.openpnp.machine.reference.ReferenceActuator;
import org.openpnp.machine.reference.ReferenceHead;
import org.openpnp.machine.reference.ReferenceHeadMountable;
import org.openpnp.machine.reference.ReferenceNozzle;
import org.openpnp.machine.reference.driver.wizards.AbstractSerialPortDriverConfigurationWizard;
import org.openpnp.model.LengthUnit;
import org.openpnp.model.Location;
import org.openpnp.model.Part;
import org.openpnp.spi.PropertySheetHolder;
import org.simpleframework.xml.Attribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiHeadMultiRelayDriver extends AbstractSerialPortDriver implements Runnable{
	private static final Logger logger = LoggerFactory.getLogger(MultiHeadMultiRelayDriver.class);
	private static final double minimumRequiredVersion = 0.81;
	private double connectedVersion;
	private boolean disconnectRequested;
	
	private Object commandLock = new Object();
	
	private Nozzle nozzles[] = {new Nozzle(),new Nozzle(),new Nozzle(),new Nozzle(),new Nozzle(),new Nozzle()}; 
	private class Nozzle{
		double x = 0;
		double y = 0;
		double z = 0;
		double c = 0; //rotation
		double f = 0; //speed
		boolean vacoom = false;
		public void setZero(){
			x = y = z = c = f = 0;
			vacoom = false;
		}
	}
//	private double x, y, z, c, f=0;
	private boolean tool=false;
    @Attribute(required=false)
    private int mm=0;
    @Attribute(required=false)
    private double delay=0;
    @Attribute(required=false)
    private double delay0=0;
    @Attribute(required=false)
    private double delay1=0;
    @Attribute(required=false)
    private double delay2=0;
    @Attribute(required=false)
    private double delay3=0;
    @Attribute(required=false)
    private int autoHeight=999;
    @Attribute(required=false)
    private int pickM=0x89;
//    private int vacuum = 0;
    private boolean connected;
    private Thread readerThread;
        
    @Attribute(required=false)
    private double zCamRadius = 24;
    
    @Attribute(required=false)
    private double zCamWheelRadius = 9.5;
    
    @Attribute(required=false)
    private double zGap = 2;
    
	private Queue<String> responseQueue = new ConcurrentLinkedQueue<String>();
	
	@Override
	public void home(ReferenceHead head) throws Exception {
		sendCommand("G28"); //Go to Pre-Defined Position
        sendCommand("$H");  //Run homing cycle 
        sendCommand("G21"); //Millimeter mode ( default ) : passed coordinates will be considered as millimeters (command is modal)
        sendCommand("M11");
        sendCommand("M22");
        sendCommand("M24");
        sendCommand("M26");
        sendCommand("M28");
        sendCommand("G92 X0 Y0 Z0 C0",700);//Coordinate offset. Change the current coordinates without moving 	e.g. "G92 x0 y0 z0" makes the current position a temporary home position
        for(int i=0; i<nozzles.length; i++){
        	nozzles[i].setZero();
        }
	}
	private int getNozzle(ReferenceHeadMountable head){
		ReferenceNozzle nozzler = null;
        if (head instanceof ReferenceNozzle)             
        	nozzler = (ReferenceNozzle) head;
        int nozzle = 1;
        if(nozzler!=null)
        	nozzle = Integer.parseInt(nozzler.getName().substring(1));
		return nozzle;
	}
	private int getNozzle(ReferenceNozzle nozzler){
        int nozzle = 1;
        if(nozzler!=null)
        	nozzle = Integer.parseInt(nozzler.getName().substring(1));
		return nozzle;
	}
	@Override
	public void moveTo(ReferenceHeadMountable head, Location loc, double speed) throws Exception {
		moveTo(getNozzle(head), loc.getX(), loc.getY(), loc.getZ(), loc.getRotation(), speed);
	}
	private void moveTo(int nozzle, double locX, double locY, double locZ, double locC, double speed) throws Exception{
        //TODO: what is pickM
    	if(pickM!=0) nozzle=1;
    	
		//if input values are not defined take old positions
		if (Double.isNaN(locX)||Double.isInfinite(locX))   locX = nozzles[nozzle].x;
		if (Double.isNaN(locY)||Double.isInfinite(locY))   locY = nozzles[nozzle].y;
		if (Double.isNaN(locZ)||Double.isInfinite(locZ))   locZ = nozzles[nozzle].z;
		if (Double.isNaN(locC)||Double.isInfinite(locC))   locC = nozzles[nozzle].c;
		if (Double.isNaN(speed)||Double.isInfinite(speed))   speed = nozzles[nozzle].f;
		//if values are defined but out of range correct them to last values for safety
		if(locX< -999.9||locX>999.9) locX=nozzles[nozzle].x;
		if(locY< -999.9||locY>999.9) locY=nozzles[nozzle].y;
		if(locZ< -999.9||locZ>999.9) locZ=nozzles[nozzle].z;
		if(locC< -999.9||locC>999.9) locC=nozzles[nozzle].c;
		if(speed==-1) speed=0;
		if(speed<0||speed>99999) speed=nozzles[nozzle].f;

        if(speed!=0)
        	nozzles[nozzle].f=speed;

	    //System.out.println("MoveTo("+head+","+x+","+y+","+z+","+c+","+feedRateMmPerMinute+")");
		if(autoHeight!=999) {
	        if(nozzles[nozzle].vacoom==false)
                if(locZ<autoHeight&&nozzles[nozzle].z>autoHeight)   // down
                    moveTo_(nozzle, nozzles[nozzle].x,nozzles[nozzle].y,autoHeight,locC,0);
                else
	                if(locZ<autoHeight) ;
	                else speed=0;
/*
                if(z>autoHeight&&this.z<autoHeight)   // up
                    moveTo(head,this.x,this.y,autoHeight+0.1,this.c,f),f=0;
                if(z>autoHeight) f=0;
*/
		}

        // make it first.
        // Also, since C is so slow in comparison, we just increase it
        // by a factor of 10.

        if (locC != nozzles[nozzle].c){
	        if(speed!=0){
	        	//check if path for moving xyz is longer than rotating C
		        if(Math.sqrt(Math.pow((locX-nozzles[nozzle].x),2)+Math.pow((locY-nozzles[nozzle].y),2)+Math.pow((locZ-nozzles[nozzle].z),2))
		        		<= Math.sqrt(Math.pow((locC-nozzles[nozzle].c),2))){
		        	//first rotate at 10x speed
		        	moveTo_(nozzle, nozzles[nozzle].x, nozzles[nozzle].y, nozzles[nozzle].z, locC, speed*10);
		        }
	        }
        }
        moveTo_(nozzle, locX, locY, locZ, locC, speed);
    //System.out.println("done moveTo("+head+","+this.x+","+this.y+","+this.z+","+this.c+")");

    }
    private void moveTo_(int nozzle, double locX, double locY, double locZ, double locC, double speed)
            throws Exception {

        StringBuffer sb = new StringBuffer();
        if (locX != nozzles[nozzle].x) {
            sb.append(String.format("X%2.2f ", locX));
        }
        if (locY != nozzles[nozzle].y) {
            sb.append(String.format("Y%2.2f ", locY));
        }
        if (locZ != nozzles[nozzle].z) {
            double a;
            a = Math.toDegrees(Math.asin((locZ - zCamWheelRadius - zGap) / zCamRadius));
            logger.debug("nozzle {} {} {}", new Object[] { locZ, zCamRadius, a });
            if (tool) a*=-1;
    	if(pickM!=0) 
            sb.append(String.format("Z%2.2f ", locZ));
    	else
            sb.append(String.format("A%2.2f ", a));
        }
        if (locC != nozzles[nozzle].c) {
            // TODO see above bug note, and remove this when fixed.
            sb.append(String.format("%c%2.2f ",tool?'B':'C', locC));
        }
        if (sb.length() > 0) {
            if(speed!=0)
            sb.append(String.format("F%2.2f", speed));
            if(speed!=0)
                sendCommand("G1 " + sb.toString());
            else
                sendCommand("G0 " + sb.toString());
            // dwell();
        }
        nozzles[nozzle].x = locX;
        nozzles[nozzle].y = locY;
        nozzles[nozzle].z  = locZ;
        nozzles[nozzle].c  = locC;
/*
        if(head!=null)
            head.updateDuringMoveTo(this.x, this.y, this.z, this.c);
*/

    //System.out.println("done moveTo("+head+","+this.x+","+this.y+","+this.z+","+this.c+")");

    }

	@Override
	public Location getLocation(ReferenceHeadMountable head) {
		int nozzle = getNozzle(head);
        return new Location(LengthUnit.Millimeters, nozzles[nozzle].x, nozzles[nozzle].y, nozzles[nozzle].z, nozzles[nozzle].c).add(head.getHeadOffsets());
	}

	@Override
	public void pick(ReferenceNozzle head) throws Exception {
		int nozzle = getNozzle(head);
        double dly=0.3+delay; 
        int m=(pickM>>4)&0xf;
        if(m==0) m=21; 
        int t=tool?2:0;
		if(nozzles[nozzle].vacoom==false) {
			switch(nozzle){
			case 1:
				sendCommand("M10"); // vacoom nozle1 ON
			}
	        sendCommand("M10"); // off
			sendCommand("G4P"+(dly+delay0));
		}
        nozzles[nozzle].vacoom = true;
        if(mm!=0)
	        if(nozzles[nozzle].y>mm&&nozzles[nozzle].y<(mm+10)) {   
	            moveTo(nozzle, nozzles[nozzle].x, nozzles[nozzle].y-40, nozzles[nozzle].z, nozzles[nozzle].c, nozzles[nozzle].f );
	            sendCommand("M"+(m+t));
	            moveTo(nozzle, nozzles[nozzle].x, nozzles[nozzle].y, nozzles[nozzle].z-5.5, nozzles[nozzle].c, nozzles[nozzle].f );
	            sendCommand("G4P"+dly);
	            moveTo(nozzle, nozzles[nozzle].x, nozzles[nozzle].y, nozzles[nozzle].z+5.5, nozzles[nozzle].c, nozzles[nozzle].f );
	            sendCommand("G4P0");
	            moveTo(nozzle, nozzles[nozzle].x, nozzles[nozzle].y+40, nozzles[nozzle].z, nozzles[nozzle].c, nozzles[nozzle].f );
	            sendCommand("G4P0");
	        } else {
	            sendCommand("M"+(m+t));
	            sendCommand("G4P"+dly);
          } else {
            sendCommand("M"+(m+t));
	    sendCommand("G4P"+dly);
	  }
        //dwell();
    }
    
	@Override
	public void place(ReferenceNozzle nozzle) throws Exception {
        double dly=0.3;
        dwell();
        int m1=(pickM>>4)&0xf , m2=pickM&0xf , m=pickM;
		if(m==0||m==199) // multi head
		{ int t=tool?2:0; m1=21+1+t; m2=21+4+t; if(m==199) { m1=9; m2=7;}
			place(nozzle,m1,delay1,m2,delay2,m2+1,delay3);
		} else { // single head
			place(nozzle,m1,delay1,m1,delay1,0,0);
		}
       }

	private void place(ReferenceNozzle nozzle,int m1, double dly1, int m2, double dly2, int m3, double dly3) throws Exception {
	if(delay2<=delay1) { dly1-=dly2; 
	if(dly3<0) dly3=0; if(dly2<0) dly2=0; if(dly1<0) dly1=0;
		if(m2>0) {
			sendCommand("G4P"+dly2);
        		sendCommand("M"+m2); 
		}
		if(m1>0) {
			sendCommand("G4P"+dly1);
        		sendCommand("M"+m1); 
		}
		if(m3>0) {
        		sendCommand("M"+m3); 
			sendCommand("G4P"+dly3);
		}
	} else  place(nozzle,m2,dly2,m1,dly1,m3,dly3);

	dwell();
       }

	@Override
	public void actuate(ReferenceActuator actuator, boolean on) throws Exception {
		actuate(actuator,0,on);
	}
	@Override
	public void actuate(ReferenceActuator actuator, double value) throws Exception {
		actuate(actuator,0,value!=0.0);
	}
    private void actuate(ReferenceActuator head, int index, boolean on)
            throws Exception {
        if (index == 0)
            sendCommand(on ? "M7" : "M9");
        if (index == 1)
            sendCommand(on ? "M8" : "M9");
        if (index == 2)
            sendCommand(on ? "M10" : "M11");
        if (index == 3)
            sendCommand(on ? "M3" : "M5");
        if (index == 4)
            sendCommand(on ? "M4" : "M5");
        dwell();
    }

	@Override
	public void setEnabled(boolean enabled) throws Exception {
	    if (enabled && !connected) {
	        connect();
	    }
	    if (connected) {
	        if (enabled) {
	            //sendCommand("$X");
	            sendCommand("\030\030\030",1000);
	            sendCommand("\n",500);
	            sendCommand("\n",1000);
	            sendCommand("G92 X0 Y0 Z0 C0",700);
//	            sendCommand("$1000=" + (enabled ? "1" : "0"));
			}
			dwell();
	        sendCommand("M11");
	        sendCommand("M22");
	        sendCommand("M24");
	        sendCommand("M26");
	        sendCommand("M28");
			dwell();
        }
    
        for(int i=0; i<nozzles.length; i++){
        	nozzles[i].setZero();
        }
	}

	@Override
	public Wizard getConfigurationWizard() {
		return new AbstractSerialPortDriverConfigurationWizard(this);
	}

	@Override
	public String getPropertySheetHolderTitle() {
		return getClass().getSimpleName();
	}

	@Override
	public PropertySheetHolder[] getChildPropertySheetHolders() {
		return null;
	}

	@Override
	public PropertySheet[] getPropertySheets() {
		return new PropertySheet[] {
                new PropertySheetWizardAdapter(getConfigurationWizard())
        };
	}

	@Override
	public Action[] getPropertySheetHolderActions() {
		return null;
	}

/////////////////////////////////////////////////////////////////
	private List<String> sendCommand(String command) throws Exception {
		return sendCommand(command, -1);
	}
	
	private List<String> sendCommand(String command, long timeout) throws Exception {
		synchronized (commandLock) {
			if (command != null) {
				logger.debug("sendCommand({}, {})", command, timeout);
				logger.debug(">> " + command);
				output.write(command.getBytes());
				output.write("\n".getBytes());
			}
			if (timeout == -1) {
				commandLock.wait();
			}
			else {
				commandLock.wait(timeout);
			}
		}
		List<String> responses = drainResponseQueue();
		return responses;
	}
	private List<String> drainResponseQueue() {
		List<String> responses = new ArrayList<String>();
		String response;
		while ((response = responseQueue.poll()) != null) {
			responses.add(response);
		}
		return responses;
	}
    /**
     * Causes Grbl to block until all commands are complete.
     * @throws Exception
     */
    private void dwell() throws Exception {
        sendCommand("G4 P0.01");
    }
    
    public synchronized void connect()
            throws Exception {
        disconnect();
        System.out.println("Connect");
        super.connect();
        System.out.println("Connected");

        /**
         * Connection process notes:
         *
         * On some platforms, as soon as we open the serial port it will reset
         * Grbl and we'll start getting some data. On others, Grbl may already
         * be running and we will get nothing on connect.
         */
       
        List<String> responses;
        synchronized (commandLock) {
            // Start the reader thread with the commandLock held. This will
            // keep the thread from quickly parsing any responses messages
            // and notifying before we get a change to wait.
            readerThread = new Thread(this);
            readerThread.start();
            // Wait up to 3 seconds for Grbl to say Hi
            // If we get anything at this point it will have been the settings
            // dump that is sent after reset.
            responses = sendCommand(null, 3000);
        }

        processConnectionResponses(responses);

        for (int i = 0; i < 5 && !connected; i++) {
            responses = sendCommand("\030", 5000);
            processConnectionResponses(responses);
        }
       
        if (!connected)  {
            throw new Error(
                String.format("Unable to receive connection response from Grbl. Check your port and baud rate, and that you are running at least version %f of Grbl",
                        minimumRequiredVersion));
        }
       
        if (connectedVersion < minimumRequiredVersion) {
            throw new Error(String.format("This driver requires Grbl version %.2f or higher. You are running version %.2f", minimumRequiredVersion, connectedVersion));
        }
       
        // We are connected to at least the minimum required version now
        // So perform some setup
       
        // Turn off the stepper drivers
        setEnabled(false);
       
        // Reset all axes to 0, in case the firmware was not reset on
        // connect.
        sendCommand("G92 X0 Y0 Z0 C0");
    }
   
    private void processConnectionResponses(List<String> responses) {
        for (String response : responses) {
        	if(true) {
	            if (response.startsWith("$VERSION = ")) {
	                String[] versionComponents = response.split(" ");
	                connectedVersion = Double.parseDouble(versionComponents[2]);
	                connected = true;
	                logger.debug(String.format("Connected to Grbl Version: %.2f", connectedVersion));
	            }
			} else {
	                connectedVersion = 0.9;
	                connected = true;
			}
        }
    }
    public synchronized void disconnect(){
        disconnectRequested = true;
        connected = false;
        System.out.println("DisConnect");
        try {
            if (readerThread != null && readerThread.isAlive()) {
                readerThread.join();
            }
        }
        catch (Exception e) {
            logger.error("disconnect()", e);
        }
        try {
			super.disconnect();
		} catch (Exception e) {
			logger.error("disconnect()", e);
		}
        
        disconnectRequested = false;
    }  

    @Override
    public void run() {
        while (!disconnectRequested) {
                String line;
                try {
                    line = readLine().trim();
                }
                catch (TimeoutException ex) {
                    continue;
                }
                catch (IOException e) {
                    logger.error("Read error", e);
                    return;
                }
                line = line.trim();
                logger.debug("<< " + line);
                responseQueue.offer(line);
    if (line.equals("ok") || line.startsWith("error: ")
        || line.startsWith("$VERSION =")) {
        // This is the end of processing for a command
//System.out.println("Line = " + line);
        synchronized (commandLock) {
            commandLock.notify();
        }
    }
}
}

    
    
    
}

